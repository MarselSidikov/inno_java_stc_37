package hard;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TextProcessorImpl implements TextProcessor {
    // максимальное количество наблюдателей
    private static final int MAX_OBSERVER_OBSERVERS_COUNT = 5;
    // массив наблюдателей
    private TextObserver observers[];
    // текущее количество наблюдателей
    private int countObservers;

    public TextProcessorImpl() {
        this.observers = new TextObserver[MAX_OBSERVER_OBSERVERS_COUNT];
    }

    @Override
    public void addDocument(String document) {
        // вывели информацию о получении документа
        System.out.println("Получен документ : " + document);
        // оповестили всех обработчиков о том, что у нас новый документ
        notifyObservers(document);
    }

    @Override
    public void addObserver(TextObserver observer) {
        if (countObservers < MAX_OBSERVER_OBSERVERS_COUNT) {
            this.observers[countObservers] = observer;
            countObservers++;
        } else {
            System.err.println("Превышено допустимое количество обработчиков");
        }
    }

    @Override
    public void notifyObservers(String document) {
        // проходим по всем обработчикам, которые есть в данном списке
        for (int i = 0; i < countObservers; i++) {
            // вызываем у них метод обработки документа
            observers[i].handleDocument(document);
        }
    }
}
