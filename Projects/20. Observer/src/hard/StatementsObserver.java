package hard;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StatementsObserver implements TextObserver {

    private final static String STATEMENT_SIGN = "Заявление";

    @Override
    public void handleDocument(String document) {
        // если это заявление
        if (document.contains(STATEMENT_SIGN)) {
            System.out.println("Заявление отправлено в соответствующий отдел!");
        }
    }
}
