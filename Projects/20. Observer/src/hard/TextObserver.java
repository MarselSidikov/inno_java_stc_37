package hard;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TextObserver {
    void handleDocument(String document);
}
