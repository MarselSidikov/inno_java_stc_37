package hard;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CertificateObserver implements TextObserver {

    private final static String CERTIFICATE_SIGN = "Справка";

    @Override
    public void handleDocument(String document) {
        if (document.contains(CERTIFICATE_SIGN)) {
            System.out.println("Справка передана в печать!");
        }
    }
}
