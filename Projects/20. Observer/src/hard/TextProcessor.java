package hard;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// обработчик документов
public interface TextProcessor {
    // метод обработки массива документов
    void addDocument(String document);
    // метод добавления наблюдателя, который будет
    // реагировать на документы
    void addObserver(TextObserver observer);
    // оповещение наблюдателей о наличии какого-то документа
    void notifyObservers(String document);
}
