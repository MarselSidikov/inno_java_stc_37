package hard;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * 04.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        TextProcessor textProcessor = new TextProcessorImpl();
        textProcessor.addObserver(new StatementsObserver());
        textProcessor.addObserver(new CertificateObserver());
        textProcessor.addObserver(document -> {
            System.err.println("Документ <" + document.split(" ")[0] + "> получен в " + LocalTime.now().getHour() + ":"
                    + LocalTime.now().getMinute() + ":" + LocalTime.now().getSecond());
        });

        while (true) {
            // считали документ
            String document = scanner.nextLine();
            // добавили его в обработчик
            textProcessor.addDocument(document);
        }
    }
}
