package lite;

/**
 * 02.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Observable - наблюдаемый, за ним наблюдают
public interface Button {
    void onClick(ClickReaction reaction);
    void click();
}
