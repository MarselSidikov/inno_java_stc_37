package lite;

/**
 * 02.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ExitButton exitButton = new ExitButton();
        exitButton.onClick(() -> System.exit(-100));
        exitButton.click();
    }
}
