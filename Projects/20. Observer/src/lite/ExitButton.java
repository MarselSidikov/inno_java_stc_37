package lite;

/**
 * 02.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ExitButton implements Button {

    private ClickReaction reaction;

    // здесь мы пишем, какой наблюдатель должен следить за кнопкой
    @Override
    public void onClick(ClickReaction reaction) {
        this.reaction = reaction;
    }

    @Override
    public void click() {
        // делаем какую-то работу
        System.out.println("Выходим из приложения");
        // оповещаем наблюдателя о событии
        reaction.handle();
    }
}
