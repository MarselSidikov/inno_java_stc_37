package lite;

/**
 * 02.03.2021
 * 20. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Observer
public interface ClickReaction {
    void handle();
}
