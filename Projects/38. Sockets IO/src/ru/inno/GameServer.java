package ru.inno;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 22.04.2021
 * 38. Sockets IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// сервер (класс)
public class GameServer {

    // отдельный поток для первого игрока (параллельный main)
    private ClientThread firstPlayer;
    // отдельный поток для второго игрока (параллельный main)
    private ClientThread secondPlayer;
    // объект для сокет-сервера
    private ServerSocket serverSocket;


    // метод запуска сервера на определенном порту
    public void start(int port) {
        try {
            // запустили SocketServer на определенном порту
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            // ждем первого
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            firstPlayer = connect();
            // ждем второго
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        Socket client;
        // получили клиента
        try {
            // уводит приложение в ожидание (wait) пока не присоединиться какой-либо клиент
            // как только клиент подключен к серверу
            // объект-соединение возвращается как результат выполнения метода
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        // создали ему отдельный поток
        ClientThread clientThread = new ClientThread(client);
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        // отправили клиенту сообщение о том, что он подключен
        clientThread.sendMessage("Вы подключены к серверу");
        return clientThread;
    }

    // отдельный поток для клиента
    private class ClientThread extends Thread {
        // что мы хотим отправить клиенту
        private PrintWriter toClient;
        // что мы хотим получить от клиента
        private BufferedReader fromClient;

        public ClientThread(Socket client) {
            try {
                // autoflush - чтобы сразу отправлял данные в поток, а не ждал пока
                // не вызовут принудительно flush
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public void run() {
            // бесконечно работающий клиентский поток
            // ожидает сообщения от клиента на сервере
            while (true) {
                String messageFromClient;
                try {
                    // получили сообщение от клиента
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                if (messageFromClient != null) {
                    if (meFirst()) {
                        System.out.println("ОТ ПЕРВОГО ИГРОКА: " + messageFromClient);
                        secondPlayer.sendMessage(messageFromClient);
                    } else {
                        System.out.println("ОТ ВТОРОГО ИГРОКА: " + messageFromClient);
                        firstPlayer.sendMessage(messageFromClient);
                    }
                }
            }
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }
    }
}
