package collections;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        InnoList<Integer> integersList = new InnoLinkedList<>();
        integersList.add(10);
        integersList.add(15);
        integersList.add(100);
//        integersList.add("Hello");

        int n1 = integersList.get(0);
        int n2 = integersList.get(1);
        int n3 = integersList.get(2);
//        String value = integersList.get(0);

        InnoIterator<Integer> integerIterator = integersList.iterator();

        while (integerIterator.hasNext()) {
            int currentNumber = integerIterator.next();
            System.out.println(currentNumber);
        }

        InnoList<String> stringsList = new InnoArrayList<>();
        stringsList.add("Hello!");
        stringsList.add("Bye");
        stringsList.add("Marsel");
//        stringsList.add(1);

        String value1 = stringsList.get(0);
        String value2 = stringsList.get(1);
        String value3 = stringsList.get(2);
//        int value = stringsList.get(0);

        InnoIterator<String> innoIterator = stringsList.iterator();

        while (innoIterator.hasNext()) {
            String currentString = innoIterator.next();
            System.out.println(currentString);
        }
    }
}
