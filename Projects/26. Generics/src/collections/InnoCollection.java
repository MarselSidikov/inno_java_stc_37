package collections;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Обобщенная коллекция, здесь описаны методы, которые работают для
 * коллекции, содержащей любые элементы
 * @param <A> -> параметр, куда мы подставляем тип
 */
public interface InnoCollection<A> {
    /**
     * Добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(A element);

    /**
     * Удаляет элемент из коллекции
     * @param element удаляемый элемент
     */
    void remove(A element);

    /**
     * Проверяет наличие элемента в коллекции
     * @param element искомый элемент
     * @return true, если элемент найден, false в противном случае
     */
    boolean contains(A element);

    /**
     * Количество элементов коллекции
     * @return значение, равное количеству элементов
     */
    int size();

    /**
     * Метод для получения итератора текущей коллекции
     * @return объект-итератор
     */
    InnoIterator<A> iterator();
}
