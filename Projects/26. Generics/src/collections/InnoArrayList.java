package collections;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class InnoArrayList<C> implements InnoList<C> {

    private static final int DEFAULT_SIZE = 10;
    // обощенный массив
    private C elements[];

    private int count;

    public InnoArrayList() {
        // в Java нельзя создать обобщенный массив
        // создаем массив object-ов
        // преобразуем его к C[]
        this.elements = (C[])new Object[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public C get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return null;
        }
    }

    @Override
    public void insert(int index, C element) {
        // TODO: реализовать
    }

    @Override
    public void addToBegin(C element) {
        // TODO: реализовать
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: реализовать
    }

    @Override
    public void add(C element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        C newElements[] = (C[])new Object[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(C element) {
        // TODO: реализовать
    }

    @Override
    public boolean contains(C element) {
        // TODO: реализовать
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator<C> iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator<C> {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public C next() {
            // берем значение под текущей позицией итератора
            C nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
