package methods;

import java.util.ArrayList;
import java.util.List;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Processor {
    public <X, Y> List<Y> process(List<X> list, ProcessFunction<X, Y> function) {
        List<Y> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            X x = list.get(i);
            Y y = function.process(x);
            result.add(y);
        }

        return result;
    }
}
