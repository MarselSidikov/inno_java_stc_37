package methods;

import java.util.ArrayList;
import java.util.List;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Processor processor = new Processor();
        List<String> lines = new ArrayList<>();
        lines.add("Привет");
        lines.add("Как дела?");
        lines.add("Что нового?");
        lines.add("Расскажи!");

        List<Integer> lenghts = processor.process(lines, line -> line.length());
        List<Character> firstLetters = processor.process(lines, line -> line.charAt(0));
        System.out.println(lenghts);
        System.out.println(firstLetters);
    }
}
