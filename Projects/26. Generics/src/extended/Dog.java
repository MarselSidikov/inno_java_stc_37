package extended;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Dog extends Wolf {
    @Override
    public String toString() {
        return "Собака!";
    }
}
