package intro;

/**
 * 15.03.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class iPhone implements Phone {
    @Override
    public String getModel() {
        return "intro.iPhone";
    }

    public void createPhoto() {
        System.out.println("Сделали фотографию!");
    }
}
