package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class Transport {
    private static final double DEFAULT_FUEL_AMOUNT = 0.1;
    private static final double DEFAULT_FUEL_CONSUMPTION = 0.1;
    // поле - объем топлива
    protected double fuelAmount = DEFAULT_FUEL_AMOUNT;
    // поле - потребление топлива
    protected double fuelConsumption = DEFAULT_FUEL_CONSUMPTION;

    public Transport(double fuelAmount, double fuelConsumption) {
        if (fuelAmount > 0) {
            this.fuelAmount = fuelAmount;
        }
        if (fuelConsumption > 0) {
            this.fuelConsumption = fuelConsumption;
        }
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public abstract void go(int km);
}
