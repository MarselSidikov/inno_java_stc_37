package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Tank tank = new Tank(100, 2, 10);
        Airplane airplane = new Airplane(350, 4, 30);
        MilitaryAirplane militaryAirplane = new MilitaryAirplane(400, 10, 500);

        // преобразование типа объектной переменной
        // tank - объектная переменная класса-потомка
        // t1 - объектная переменная класса-предка
        // t1 может ссылаться на объект типа example1.Tank

        // восходящее преобразование, неявное
        Transport t1 = tank;
        Transport t2 = airplane;
        Transport t3 = militaryAirplane;
//
//        t1.go(100);
//        t2.go(100);
//        t3.go(100);

        Transport transports[] = {t1, t2, t3};
        for (int i = 0; i < transports.length; i++) {
            transports[i].go(100);
        }

    }
}
