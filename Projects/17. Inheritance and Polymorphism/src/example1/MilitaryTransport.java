package example1;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class MilitaryTransport extends Transport {
    protected int bulletsCount;


    public MilitaryTransport(double fuelAmount, double fuelConsumption, int bulletsCount) {
        super(fuelAmount, fuelConsumption);
        this.bulletsCount = bulletsCount;
    }

    public abstract void fire();

    public int getBulletsCount() {
        return bulletsCount;
    }
}
