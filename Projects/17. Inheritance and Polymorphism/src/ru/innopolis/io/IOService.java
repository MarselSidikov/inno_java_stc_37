package ru.innopolis.io;

import ru.innopolis.io.base.DeviceInput;
import ru.innopolis.io.base.DeviceOutput;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// фасад - обернуть функциональность нескольких разных классов
// в один класс
public class IOService {

    private DeviceInput input;
    private DeviceOutput output;

    public IOService(DeviceInput input, DeviceOutput output) {
        this.input = input;
        this.output = output;
    }

    void print(String message) {
        output.print(message);
    }

    String read() {
        return input.read();
    }

    void printDevicesInformation() {
        input.printInfo();
        output.printInfo();
    }
}
