package ru.innopolis.io.impl;

import ru.innopolis.io.base.DeviceOutput;
import ru.innopolis.io.base.Stream;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceOutputErrorImpl implements DeviceOutput, Stream {
    @Override
    public void print(String message) {
        System.err.println(message);
    }

    @Override
    public void printInfo() {
        System.out.println("Реализация выходного потока на основе System.err");
    }

    @Override
    public void printInfoAboutStream() {
        System.out.println("Используется System.err");
    }
}
