package ru.innopolis.io.impl;

import ru.innopolis.io.base.DeviceOutput;
import ru.innopolis.io.base.Stream;

import java.time.LocalTime;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceOutputWithTimeImpl implements DeviceOutput, Stream {

    @Override
    public void print(String message) {
        System.out.println("Сообщение в  " + LocalTime.now() + " [" + message + "]");
    }

    @Override
    public void printInfo() {
        System.out.println("Реализация выходного устройства со временем");
    }

    @Override
    public void printInfoAboutStream() {
        System.out.println("Используется System.out");
    }
}
