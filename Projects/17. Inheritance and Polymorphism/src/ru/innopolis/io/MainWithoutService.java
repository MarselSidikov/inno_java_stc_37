package ru.innopolis.io;

import ru.innopolis.io.base.DeviceInput;
import ru.innopolis.io.base.DeviceOutput;
import ru.innopolis.io.impl.DeviceInputWithTimeScannerImpl;
import ru.innopolis.io.impl.DeviceOutputErrorImpl;
import ru.innopolis.io.impl.DeviceOutputWithTimeImpl;

/**
 * 26.02.2021
 * 17. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
/* реализовать гибкую систему ввода-вывода
 * считывать и выводить данные нужно из разных источников в разные источники и по-разному
 */
public class MainWithoutService {
    public static void main(String[] args) {
//        DeviceInput input = new DeviceInputWithPrefixScannerImpl("СЧИТАНО С КОНСОЛИ: ");
        DeviceInput input = new DeviceInputWithTimeScannerImpl();
//        DeviceOutput output = new DeviceOutputWithTimeImpl();
        DeviceOutput output = new DeviceOutputErrorImpl();
        input.printInfo();
        output.printInfo();
        String text = input.read();
        output.print(text);
    }
}
