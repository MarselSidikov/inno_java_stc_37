package version2;

/**
 * 02.03.2021
 * 19. Anons and Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// интерфейс, в котором определена функция для обработки чисел
// у данного интерфейса есть только один метод для реализации
// такой интерфейс называется ФУНКЦИОНАЛЬНЫМ
public interface ProcessFunction {
    int process(int number);
}
