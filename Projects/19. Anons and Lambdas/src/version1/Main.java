package version1;

public class Main {

    public static void main(String[] args) {
	    // вложенный класс - анонимный класс
    	NumbersUtil numbersUtil = new NumbersUtil() {
			@Override
			protected int processNumber(int number) {
				int processedNumber = 1;
				while (number != 0) {
					processedNumber = processedNumber * (number % 10);
					number /= 10;
				}
				return processedNumber;
			}
		};

    	NumbersUtil numbersUtil1 = new NumbersUtil() {
			@Override
			protected int processNumber(int number) {
				return number % 3;
			}
		};

	    numbersUtil.process(1234);
	    numbersUtil.process(777);
	    numbersUtil.process(111);
	    numbersUtil.process(115);
	    numbersUtil.showProcessed();

		System.out.println();

		numbersUtil1.process(1234);
		numbersUtil1.process(777);
		numbersUtil1.process(111);
		numbersUtil1.process(115);
		numbersUtil1.showProcessed();
    }
}
