import java.util.*;

public class Main {

    public static void main(String[] args) {
	    List<String> list = new LinkedList<>();
	    list.add("Марсель");
	    list.add("Виктор");
	    list.add("Даниил");
	    list.add("Айрат");

	    // потому что List -> Collection -> Iterable

	    Iterable<String> iterable = list;

//	    while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }

//		for (String s : iterable) {
//			System.out.println(s);
//		}

	    Collection<String> collection = list;

		Map<String, String> map = new HashMap<>();
		map.put("Марсель", "Сидиков");
		map.put("Даниил", "Вдовинов");
		map.put("Айрат", "Мухутдинов");
		map.put("Виктор", "Евлампьев");
		map.put("Алия", "Мухутдинова");
		map.put("Максим", "Поздеев");

//		System.out.println(map.containsKey("Виктор"));
//		System.out.println(map.containsKey("Игорь"));
//		System.out.println(map.containsValue("Поздеев"));
//		System.out.println(map.containsValue("Акинфеев"));
//		System.out.println(map.get("Максим"));
//		System.out.println(map.get("Игорь"));

		// Set<K> keySet();
		// Collection<V> values();
		// Set<Map.Entry<K, V>> entrySet();

		// ключи уникальные, поэтому ключи возвращаются в виде множества
		Set<String> keys = map.keySet();

		for (String key : keys) {
			System.out.println(key);
		}

		Collection<String> values = map.values();

		for (String value : values) {
			System.out.println(value);
		}

		Set<Map.Entry<String, String>> entries = map.entrySet();

		for (Map.Entry<String, String> entry : entries) {
			System.out.println("Имя" + " " + entry.getKey());
			System.out.println("Фамилия" + " " + entry.getValue());
		}

    }
}
