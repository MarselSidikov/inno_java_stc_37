import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * 22.03.2021
 * 30. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Double d1 = 0.1;
        Double d2 = 0.2;
        Double d3 = 0.3;
        Double d4 = 0.4;
        Double d5 = 0.5;
        Double d6 = 0.6;
        Double d7 = 0.7;
        Double d8 = 0.8;
        Double d9 = 0.9;

        HashMap<Double, String> map = new HashMap<>();
        map.put(d1, "Марсель");
        map.put(d2, "Виктор");

        System.out.printf("%14d%10s%32s\n", d1.hashCode(), " in bin ", Integer.toBinaryString(d1.hashCode()));
        System.out.printf("%14d%10s%32s\n", d2.hashCode(), " in bin ", Integer.toBinaryString(d2.hashCode()));
        System.out.printf("%14d%10s%32s\n", d3.hashCode(), " in bin ", Integer.toBinaryString(d3.hashCode()));
        System.out.printf("%14d%10s%32s\n", d4.hashCode(), " in bin ", Integer.toBinaryString(d4.hashCode()));
        System.out.printf("%14d%10s%32s\n", d5.hashCode(), " in bin ", Integer.toBinaryString(d5.hashCode()));
        System.out.printf("%14d%10s%32s\n", d6.hashCode(), " in bin ", Integer.toBinaryString(d6.hashCode()));
        System.out.printf("%14d%10s%32s\n", d7.hashCode(), " in bin ", Integer.toBinaryString(d7.hashCode()));
        System.out.printf("%14d%10s%32s\n", d8.hashCode(), " in bin ", Integer.toBinaryString(d8.hashCode()));
        System.out.printf("%14d%10s%32s\n", d9.hashCode(), " in bin ", Integer.toBinaryString(d9.hashCode()));

        Set<String> strings = new HashSet<>();
        strings.add("Марсель");
        strings.add("Марсель");
        strings.add("Марсель");
        System.out.println(strings.size());

    }
}
