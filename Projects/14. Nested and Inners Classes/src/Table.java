/**
 * 24.02.2021
 * 14. Nested and Inners Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Table {
    private static final int MAX_ENTRIES_COUNT = 10;

    private Entry entries[];

    private int count;

    // ширина столбца для ключа
    private int keyWidth;
    // ширина столбца для значения
    private int valueWidth;

    public Table(int keyWidth, int valueWidth) {
        this.entries = new Entry[MAX_ENTRIES_COUNT];
        this.keyWidth = keyWidth;
        this.valueWidth = valueWidth;
    }

    public void add(String key, int value) {
        Entry entry = new Entry(key, value);

        if (count < MAX_ENTRIES_COUNT) {
            entries[count] = entry;
            count++;
        } else {
            System.err.println("Больше добавить нельзя");
        }
    }

    public void print() {

    }

    // статический вложенный класс - вложенный
    // данный класс помещается внутри внешнего и ассоциирован с ним
    // чтобы создать экземпляр данного класса вне внешнего класса
    // нужно указать имя внешнего класса

    // почему мы его поместили сюда?
    // потому что удобнее держать класс, который используется только в Table внутри Table

    // вложенный класс не имеет доступа к нестатическим членам внешнего класса
    private static class Entry {
        String key;
        int value;

        Entry(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    // внутренний (без static)
    // ассоциирован с объектом
    // внутренний класс имеет доступ к объекту внутри которого он будет создан
    public class TablePrinter {
        public void print() {
            System.out.printf("%" + keyWidth + "s" + "|" + "%" + valueWidth + "s|\n", "Ключ", "Значение");
            for (int i = 0; i < count; i++) {
                System.out.printf("%" + keyWidth + "s" + "|" + "%" + valueWidth + "d|\n", entries[i].key, entries[i].value);
            }
        }
    }
}
