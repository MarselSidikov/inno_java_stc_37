import java.util.Arrays;

class Program4 {
	// Selection
	public static void main(String[] args) {
		int numbers[] = {3, 2, 10, -5, 15, 18, -50};

		System.out.println(Arrays.toString(numbers));


		int temp;
		int min;
		int positionOfMin;

		for (int i = 0; i < numbers.length; i++) {
			min = numbers[i];
			positionOfMin = i;

			for (int j = i; j < numbers.length; j++) {
				if (numbers[j] < min) {
					min = numbers[j];
					positionOfMin = j;
				}
			}

			temp = numbers[i];
			numbers[i] = numbers[positionOfMin];
			numbers[positionOfMin] = temp;
			System.out.println(Arrays.toString(numbers));
		}		
	}
}