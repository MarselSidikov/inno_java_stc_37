import java.util.Scanner;
import java.util.Arrays;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int numbers[] = {3, 2, 10, -5, 15, 18, -50};

		System.out.println(Arrays.toString(numbers));

		int numberForSearch = scanner.nextInt();
		int position = -1;

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == numberForSearch) {
				position = i;
				break;
			}
		}

		System.out.println(position);

	}
}