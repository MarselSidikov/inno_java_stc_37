package ru.inno.base;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
	    Scanner scanner = new Scanner(System.in);
	    scanner.nextLine();

	    HenThread henThread = new HenThread("Галина");
        HenThread henThread1 = new HenThread("Бланка");
        EggThread eggThread = new EggThread("Красное");
	    EggThread eggThread1 = new EggThread("Голубое");

		henThread.start();
		eggThread.start();
	    henThread1.start();
	    eggThread1.start();
	    new Thread(new Tirex()).start();

	    // henThread - B
		// main - A
	    // мы в потоке A обращаемся к объекту-потоку B и вызываем у него join()
		// это означает, что поток A дождется полного завершения выполнения потока B

		try {
			henThread.join();
			eggThread.join();
			henThread1.join();
			eggThread1.join();
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}

		for (int i = 0; i < 100; i++) {
			System.out.println("Human");
		}
    }
}
