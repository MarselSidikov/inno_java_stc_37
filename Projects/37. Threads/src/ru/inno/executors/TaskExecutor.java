package ru.inno.executors;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// объекты данного интерфейса позволяют выполнять какую-либо задачу в побочном потоке
public interface TaskExecutor {
    /**
     * Запускает задачу в побочном потоке
     * @param task объект, в котором содержится реализация метода run(), и именно эта реализация представляет собой задачу, которую необходимо выполнить
     */
    void submit(Runnable task);
}
