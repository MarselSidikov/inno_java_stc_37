package ru.inno.executors;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Реализация TaskExecutor, которая для каждой задачи создает новый поток
 * Плюсы - простота реализации
 * Минусы - слишком тяжелые операции для VM по созданию и завершению потоков
 */
public class ThreadPerTaskImpl implements TaskExecutor {
    @Override
    public void submit(Runnable task) {
        // создаем новый поток с необходимой задачей
        Thread thread = new Thread(task);
        // запускаем новый поток
        thread.start();
    }
}
