package ru.inno.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 10_000_000; i++) {
            numbers.add(random.nextInt(10_000_000));
        }

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        long before = System.currentTimeMillis();
        long count = numbers.parallelStream().filter(number -> {
            if (number == 2 || number == 3) {
                return true;
            }

            for (int i = 2; i * i < number; i++) {
                if (number % i == 0) {
                    return false;
                }
            }

            return true;
        }).count();
        long after = System.currentTimeMillis();
        System.out.println((after - before) / 1000.0);
        System.out.println(count);
    }
}
