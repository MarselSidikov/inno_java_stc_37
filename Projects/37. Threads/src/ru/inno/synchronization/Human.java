package ru.inno.synchronization;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 15.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human extends Thread {
    private String name;
    private final CreditCard creditCard;

    private static Lock lock = new ReentrantLock();

    public Human(String name, CreditCard card) {
        super();
        this.name = name;
        this.creditCard = card;
    }

//    @Override
//    public void run() {
//        for (int i = 0; i < 1000; i++) {
//            // означает, что этот блок кода может выполнять ТОЛЬКО ОДИН ПОТОК!
//            synchronized (creditCard) {
//                // человек идет покупать только тогда, когда есть деньги на карте
//                if (creditCard.getAmount() > 0) {
//                    System.out.println(name + " покупает...");
//                    // -> партнер по жизни может втиснуться и успеть сделать покупку (если стоит synchronized) то другой поток не вмешается!
//
//                    // покупка не прошла!
//                    if (creditCard.buy(10)) {
//                        System.out.println(name + " купил!!!!");
//                    } else {
//                        System.out.println(name + " говорит эээээээ.....");
//                    }
//                }
//            }
//
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                throw new IllegalStateException(e);
//            }
//        }
//    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            // означает, что этот блок кода может выполнять ТОЛЬКО ОДИН ПОТОК!
            lock.lock();

            // человек идет покупать только тогда, когда есть деньги на карте
            if (creditCard.getAmount() > 0) {
                System.out.println(name + " покупает...");
                // -> партнер по жизни может втиснуться и успеть сделать покупку (если стоит synchronized) то другой поток не вмешается!

                // покупка не прошла!
                if (creditCard.buy(10)) {
                    System.out.println(name + " купил!!!!");
                } else {
                    System.out.println(name + " говорит эээээээ.....");
                }
            }

            lock.unlock();

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
