package ru.inno.wait_notify;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Product {
    // true - продукт можно использовать
    // false - продукт нельзя использовать
    private boolean isReady;

    // продукт готов, когда его можно использовать
    public boolean isProduced() {
        return isReady;
    }

    // продукт использован, когда он не готов
    public boolean isConsumed() {
        return !isReady;
    }

    // готовим - значит приводим в "готовность"
    public void produce() {
        this.isReady = true;
    }

    public void consume() {
        this.isReady = false;
    }

}
