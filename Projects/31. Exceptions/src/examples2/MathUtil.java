package examples2;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MathUtil {
    public int div(int a, int b) {
        return a / b;
    }
}
