package examples;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainNoSuchElementException {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        integers.add(7);
        integers.add(8);

        Iterator<Integer> iterator = integers.iterator();
        iterator.next();
        iterator.next();
        iterator.next();
    }
}
