package examples;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutOfMemoryError {
    public static void main(String[] args) {
        int array[] = new int[Integer.MAX_VALUE];
    }
}
