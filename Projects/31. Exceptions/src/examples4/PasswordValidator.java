package examples4;

import java.io.IOException;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PasswordValidator {
    void validate(String password) throws IOException {
        if (password.length() < 7) {
            throw new IOException("Слабый пароль");
        }
    }
}
