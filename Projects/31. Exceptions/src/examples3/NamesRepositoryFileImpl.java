package examples3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 28.03.2021
 * 31. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NamesRepositoryFileImpl {
    private File file;

    public NamesRepositoryFileImpl(String fileName) {
        this.file = new File(fileName);
    }

    public List<String> findNames() throws FileNotFoundException, IOException {
        // создаем FileReader для чтения из файла
        FileReader fileReader = new FileReader(file); // FileReader -> FileInputStream -> throw new FileNotFoundException("Invalid file path");
        // создаем BufferedReader для чтения строк из файла
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        // создаем список строк
        List<String> lines = new ArrayList<>();
        // считываем первую строку из файла
        String line = bufferedReader.readLine(); // throw new IOException("Stream closed");
        // пока считанная строка не пустая
        while (line != null) {
            // добавляем ее в список
            lines.add(line);
            // считываем следующую строку
            line = bufferedReader.readLine(); // throw new IOException("Stream closed");
        }

        return lines;
    }
}
