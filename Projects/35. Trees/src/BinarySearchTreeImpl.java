import java.util.Deque;
import java.util.LinkedList;
import java.util.StringJoiner;

/**
 * 05.04.2021
 * 35. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        // если мы попали в пустое место
        if (root == null) {
            root = new Node<>(value);
        } else if (value.compareTo(root.value) < 0) {
            // если значение меньше корня
            // добавляем элемент в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }


    @Override
    public void printDfs() {
        dfs(root);
    }

    // [50, 24, 26, 27, 30]
    // stack.push(77);
    // [77, 50, 24, 26, 27, 30]
    // stack.pop() -> 77
    // [50, 24, 26, 27, 30]
    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        // кладем в начало стека
        stack.push(root);

        Node<T> current;
        // пока очередь не пустая
        while (!stack.isEmpty()) {
            // забираем узел из начала стека
            current = stack.pop();
            // если у текущего узла есть левый сын
            if (current.left != null) {
                // положим этого сына в начало стека
                stack.push(current.left);
            }
            System.out.println(current.value + " ");
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    // [50, 24, 26, 27, 30]
    // queue.add(7)
    // [7, 50, 24, 26, 27, 30]
    // queue.poll() -> 30
    // [7, 50, 24, 26, 27]
    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        // кладем в очередь корень
        queue.add(root);

        Node<T> current;
        // пока очередь не пустая
        while (!queue.isEmpty()) {
            // забираем узел из конца очереди
            current = queue.poll();
            System.out.println(current.value + " ");
            // если у текущего узла есть левый сын
            if (current.left != null) {
                // положим этого сына в начало очереди
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }

    private void dfs(Node<T> root) {
//        System.out.println("in root = " + root);
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
//        System.out.println("from root = " + root);
    }
}
