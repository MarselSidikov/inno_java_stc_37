package string;

/**
 * 13.03.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStringConcat {
    public static void main(String[] args) {
        // сформировать строку с числами от 1 до 100

//        String str = "";
//        for (int i = 0; i < 100; i++) {
//            str += "," + i;
//        }
//        System.out.println(str);

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < 100; i++) {
            stringBuilder.append(",").append(i);
        }

        String result = stringBuilder.toString();
        System.out.println(result);
    }
}
