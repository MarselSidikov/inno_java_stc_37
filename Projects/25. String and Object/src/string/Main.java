package string;

/**
 * 13.03.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";

        String s3 = new String("Hello");
        String s4 = new String("Hello");

        System.out.println(s1 == s2);
        System.out.println(s3 == s4);
        System.out.println(s1.equals(s4));

        String text = "Hello";
        String text2 = text.replace("el", "EL");
        System.out.println(text);
        System.out.println(text2);
    }
}
