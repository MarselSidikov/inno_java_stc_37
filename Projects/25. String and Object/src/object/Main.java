package object;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human h1 = new Human("Марсель", 27, 185);
        Human h2 = new Human("Марсель", 27, 185);

        System.out.println(h1 == h2);
        System.out.println(h1.equals(h2));
        System.out.println(h1.equals(h1));
        System.out.println(h1.equals(null));
        System.out.println(h1.equals(scanner));

        System.out.println(h1);
    }
}
