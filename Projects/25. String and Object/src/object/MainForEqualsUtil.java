package object;

/**
 * 13.03.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainForEqualsUtil {
    public static void main(String[] args) {
        EqualsUtil equalsUtil = new EqualsUtil();

        Human h1 = new Human("Марсель", 27, 185);
        Human h2 = new Human("Марсель", 27, 185);
        Human h3 = new Human("Марсель", 27, 185);
        Human h4 = new Human("Марсель", 27, 185);
        Human h5 = new Human("Марсель", 27, 185);

        System.out.println(equalsUtil.allEquals(h1, h2, h3, h4, h5));
    }
}
