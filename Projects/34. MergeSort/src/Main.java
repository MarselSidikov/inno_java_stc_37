import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	    SortAlgorithm algorithm = new MergeSort();
	    Sorter sorter = new Sorter();
	    sorter.setSortAlgorithm(algorithm);
	    int array[] = {12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10, 12, 55, -3, 6, 12, 66, 133, -10};
	    sorter.sort(array);
        System.out.println(Arrays.toString(array));
		System.out.println(array.length);

    }
}
