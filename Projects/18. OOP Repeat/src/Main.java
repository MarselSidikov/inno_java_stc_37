public class Main {

    public static void main(String[] args) {
        // объявил объектную переменную
        // она не содержит не содержит объект
        // она указывает на него
        // но сейчас она содержит null
        Student marsel = null;
        // создаю экземпляр класса (объект, instance)
        marsel = new Student("Марсель", 27);
        System.out.println(marsel.getAge());
        marsel.setName("Marsel Goodideiev");
        marsel.go();
        marsel.run();

        Athlete athlete = new Athlete("Евгений", 25, 10);
        athlete.run();

        Programmer worker = new Programmer("Виктор", 24);
        worker.go();
        worker.run();


    }
}
