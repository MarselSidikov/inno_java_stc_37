/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Athlete extends Human {
    private int rang;

    public Athlete(String name, int age, int rang) {
        // вызвал конструктор предка
        super(name, age);
        this.rang = rang;
    }

    @Override
    public void go() {
        System.out.println("Спортсмены не ходят, только бегают ;)");
    }

    // переопределили метод предка
    @Override
    public void run() {
        this.stepsCount += 30;
        System.out.println("Бежим. Итого шагов сделано -  " + stepsCount);
    }
}
