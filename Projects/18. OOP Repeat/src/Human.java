/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// абстрактный класс - нельзя создавать объекты
public abstract class Human {
    // глобальное поле-константа
    // final - значение изменить после инициализации нельзя
    // static - константа общая для всех объектов класса
    private static final int MIN_AGE = 0;
    // конкретные значения полей - состояние объекта
    // приватные поля
    // данные поля недоступны извне
    private String name;
    private int age;

    protected int stepsCount;

    // конструктор - инициализатор
    public Human(String name, int age) {
        // присутствуют проверки на корректность данных
        if (age >= MIN_AGE) {
            // this - показывает, что мы обращаемся к полю класса
            // чтобы не было конфликта имен
            this.age = age;
        } else {
            this.age = MIN_AGE;
        }

        this.name = name;
        this.stepsCount = 0;
    }

    // методы доступа геттеры
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    // метод доступа сеттер
    public void setName(String name) {
        this.name = name;
    }

    // методы, которые описывают поведение
    // абстрактные - без реализации
    public abstract void go();

    public abstract void run();
}
