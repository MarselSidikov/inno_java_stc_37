/**
 * 02.03.2021
 * 18. OOP Repeat
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Athlete athlete = new Athlete("Евгений", 25, 10);
        Student student = new Student("Марсель", 27);
        Programmer programmer = new Programmer("Виктор", 24);

        Human humans[] = {athlete, student, programmer};
        WorkMan workMans[] = {student, programmer};

        for (int i = 0; i < humans.length; i++) {
            humans[i].run();
        }

        for (int i = 0; i < workMans.length; i++) {
            workMans[i].doWork();
            workMans[i].stopWork();
        }

    }
}
