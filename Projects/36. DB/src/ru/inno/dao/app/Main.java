package ru.inno.dao.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/java_stc37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "qwerty007";

    //language=SQL
    private static final String SQL_SELECT_DRIVERS = "select * from driver";

    public static void main(String[] args) throws Exception {
	    Connection connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);

	    // отправить запрос в БД

        // объект, который умеет выполнять запросы
        Statement statement = connection.createStatement();
        // выполняем запрос
        // получили ResultSet - объект-итератор по результирующим строкам
        ResultSet rows = statement.executeQuery(SQL_SELECT_DRIVERS);

        while (rows.next()) {
            System.out.println(rows.getLong("id") + " " + rows.getString("first_name"));
        }

        System.out.println("Добавление водителя");

        Scanner scanner = new Scanner(System.in);
        String firstName = scanner.nextLine();

        //language=SQL
        String insertDriver = "insert into driver(first_name) values ('" + firstName + "')";
        System.out.println(insertDriver);
        statement.executeUpdate(insertDriver);
    }
}
