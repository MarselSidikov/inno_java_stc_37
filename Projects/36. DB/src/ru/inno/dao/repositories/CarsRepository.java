package ru.inno.dao.repositories;

import ru.inno.dao.models.Car;

import java.util.List;

/**
 * 12.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CarsRepository extends CrudRepository<Car> {
    List<Car> findAll(int page, int size);
}
