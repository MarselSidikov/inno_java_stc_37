package ru.inno.dao.repositories;

import java.util.List;

/**
 * 11.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// CREATE (save) READ (findById, findAll) UPDATE(update) ,DELETE (remove)
public interface CrudRepository<T> {
    List<T> findAll();
    void save(T model);
    void update(T model);
    void remove(Long id);
    T findById(Long id);
}
