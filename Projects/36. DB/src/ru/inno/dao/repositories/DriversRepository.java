package ru.inno.dao.repositories;

import ru.inno.dao.models.Driver;

import java.util.List;

/**
 * 11.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface DriversRepository extends CrudRepository<Driver> {
    List<Driver> findAllByAge(int age);
    List<Driver> findAllWithCars();
}
