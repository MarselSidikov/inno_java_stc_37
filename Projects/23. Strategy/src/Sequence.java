/**
 * 11.03.2021
 * 23. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Sequence {
    /**
     * Проверяет, есть ли элемент в последовательности
     * @param element искомый элемент
     * @return true - если элемент найден
     */
    boolean search(int element);

    int[] sequence();
}
