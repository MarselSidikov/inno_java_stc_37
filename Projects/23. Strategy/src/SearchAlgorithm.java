/**
 * 11.03.2021
 * 23. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SearchAlgorithm {
    boolean search(int element);
}
