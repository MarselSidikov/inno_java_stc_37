/**
 * 11.03.2021
 * 23. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchAlgorithmTrivial implements SearchAlgorithm {

    private Sequence sequence;

    public SearchAlgorithmTrivial(Sequence sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean search(int element) {
        int elements[] = sequence.sequence();

        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }
}
