/**
 * 04.03.2021
 * 22. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DocumentsPrinter {
    private DocumentsGenerator generator;

    public void setGenerator(DocumentsGenerator generator) {
        this.generator = generator;
    }

    public void printDocument(String text) {
        Document document = generator.generate(text);
        System.out.println("------- " + document.getTitle() + " -------");
        System.out.println(text);
    }
}
