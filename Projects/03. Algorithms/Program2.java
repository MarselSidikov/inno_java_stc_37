class Program2 {
	public static void main(String[] args) {
		int number = 12311123;

		int digitsSum = 0;

		while(number != 0) {
			digitsSum = digitsSum + number % 10; // 12311123 % 10 = 3
			number = number / 10; // 12311123 / 10 = 1231112
		} 

		System.out.println(digitsSum);
	}
}