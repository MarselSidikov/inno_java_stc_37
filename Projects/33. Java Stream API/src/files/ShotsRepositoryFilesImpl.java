package files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 01.04.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ShotsRepositoryFilesImpl implements ShotsRepository {

    private String fileName;

    private static final Function<String, Shot> shotMapper = line -> {
        String parsedLine[] = line.split("#");
        return new Shot(Long.parseLong(parsedLine[0]),
                LocalDateTime.parse(parsedLine[1]),
                Long.parseLong(parsedLine[2]),
                parsedLine[3],
                parsedLine[4]);
    };

    public ShotsRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Shot> findAll() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Shot> shots = reader
                    .lines()
                    .map(shotMapper)
                    .collect(Collectors.toList());
            reader.close();
            return shots;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Shot> findAllByShooterNickname(String nickname) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Shot> shots = reader
                    .lines()
                    .map(shotMapper)
                    .filter(shot -> shot.getShooter().equals(nickname))
                    .collect(Collectors.toList());
            reader.close();
            return shots;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
