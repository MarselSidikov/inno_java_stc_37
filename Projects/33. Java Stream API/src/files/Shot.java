package files;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * 21.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Long gameId;
    private String shooter;
    private String target;

    public Shot(Long id, LocalDateTime dateTime, Long gameId, String shooter, String target) {
        this.id = id;
        this.dateTime = dateTime;
        this.gameId = gameId;
        this.shooter = shooter;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getShooter() {
        return shooter;
    }

    public void setShooter(String shooter) {
        this.shooter = shooter;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(id, shot.id) &&
                Objects.equals(dateTime, shot.dateTime) &&
                Objects.equals(gameId, shot.gameId) &&
                Objects.equals(shooter, shot.shooter) &&
                Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, gameId, shooter, target);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Shot.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("dateTime=" + dateTime)
                .add("gameId=" + gameId)
                .add("shooter='" + shooter + "'")
                .add("target='" + target + "'")
                .toString();
    }
}
