public class Main {

    public static void main(String[] args) {
	    // сгенерировать 100 людей с разными именами/фамилиями и возрастами
		HumansGenerator generator = new HumansGenerator();
		HumansTable table = new HumansTable();
		HumanStatistics statistics = new HumanStatistics();

		Human humans[] = generator.generate(100);

		// проблема - любой пользователь класса (программист) может привести
		// объект в неправильное состояние
//		humans[55].age = -100;
		humans[55].setAge(-100);
		humans[55] = new Human("Fake", "Fakeof", -100);

		table.printTable(humans);

		int ages[] = statistics.getAgesStatistic(humans);

		table.printTable(ages);
    }
}
