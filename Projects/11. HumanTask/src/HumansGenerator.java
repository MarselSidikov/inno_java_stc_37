import java.util.Random;

/**
 * 18.02.2021
 * 11. HumanTask
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HumansGenerator {
    public Human[] generate(int count) {
        String firstNames[] = {
                "Никита",
                "Алексей",
                "Анна",
                "Руслан",
                "Дмитрий",
                "Екатерина",
                "Степан",
                "Ибрагим",
                "Иван",
                "София",
                "Даниэль",
                "Таисия",
                "Надежда",
                "Ян",
                "Александр",
                "Василиса",
                "Алексей",
                "Максим",
                "Александр",
                "Ева",
                "Фёдор",
                "Марина",
                "Роман",
                "Григорий",
                "Дарина",
                "Софья",
                "Агата",
                "Андрей",
                "Степан",
                "Виктория",
                "Софья",
                "Алексей",
                "Иван",
                "Александр",
                "Варвара",
                "Роман",
                "Георгий",
                "Александра",
                "Ксения",
                "Никита",
                "Кира",
                "Кирилл",
                "Елизавета",
                "Максим",
                "Артём",
                "Маргарита",
                "Андрей",
                "Егор",
                "Дарья",
                "Кристина",
                "Глеб",
                "Мирослава",
                "София",
                "Михаил",
                "Мария",
                "Роман",
                "Алина",
                "Кира",
                "Арсений",
                "Анастасия",
                "Екатерина",
                "Владимир",
                "Степан",
                "Влада",
                "Мирон",
                "Дарья",
                "Антон",
                "Артём",
                "Валерия",
                "Есения",
                "Иван",
                "Евгения",
                "Мария",
                "Варвара",
                "Александра",
                "Степан",
                "Елизавета",
                "Таисия",
                "Софья",
                "Дмитрий",
                "Ярослава",
                "Алиса",
                "Виктория",
                "Марьям",
                "Кирилл",
                "Иван",
                "Дарья",
                "Виктория",
                "Алиса",
                "Виктория",
                "Дмитрий",
                "Михаил",
                "Варвара",
                "Анастасия",
                "Варвара",
                "Андрей",
                "Екатерина",
                "Мария",
                "Андрей",
                "Маргарита"
        };

        String lastNames[] = {
                "Трофимов",
                "Волков",
                "Родина",
                "Романов",
                "Лебедев",
                "Авдеева",
                "Ефремов",
                "Павлов",
                "Макаров",
                "Громова",
                "Федотов",
                "Свиридова",
                "Попова",
                "Иванов",
                "Кожевников",
                "Григорьева",
                "Пахомов",
                "Волков",
                "Давыдов",
                "Зверева",
                "Чернов",
                "Скворцова",
                "Павлов",
                "Филатов",
                "Масленникова",
                "Чернова",
                "Тихомирова",
                "Кириллов",
                "Николаев",
                "Богомолова",
                "Седова",
                "Карташов",
                "Константинов",
                "Злобин",
                "Васильева",
                "Голиков",
                "Морозов",
                "Головина",
                "Кириллова",
                "Васильев",
                "Жукова",
                "Кузнецов",
                "Шарова",
                "Тихонов",
                "Лаврентьев",
                "Мухина",
                "Комиссаров",
                "Родионов",
                "Антонова",
                "Болдырева",
                "Степанов",
                "Ильинская",
                "Соболева",
                "Власов",
                "Малышева",
                "Кудрявцев",
                "Сычева",
                "Макарова",
                "Карпов",
                "Платонова",
                "Давыдова",
                "Агапов",
                "Колесников",
                "Акимова",
                "Никитин",
                "Балашова",
                "Алексеев",
                "Соколов",
                "Колесова",
                "Гришина",
                "Мальцев",
                "Пирогова",
                "Белова",
                "Потапова",
                "Гусева",
                "Богданов",
                "Федорова",
                "Александрова",
                "Ерофеева",
                "Черных",
                "Фролова",
                "Седова",
                "Игнатова",
                "Сафонова",
                "Тихонов",
                "Демин",
                "Тихонова",
                "Федорова",
                "Фролова",
                "Власова",
                "Аксенов",
                "Платонов",
                "Козина",
                "Киреева",
                "Ковалева",
                "Соколов",
                "Медведева",
                "Жарова",
                "Хохлов",
                "Коновалова"
        };

        Random random = new Random();
        Human humans[] = new Human[count];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human(firstNames[random.nextInt(100)],
                    lastNames[random.nextInt(100)], random.nextInt(100));
        }

        return humans;

    }
}
