/**
 * 18.02.2021
 * 11. HumanTask
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    String firstName;
    String lastName;
    private int age;

    // конструктор != метод

    // пустой конструктор
    public Human() {

    }

    // this - это ссылка на объект
    // конструктор с параметрами
    public Human(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        if (age >= 0 && age <= 99) {
            this.age = age;
        } else {
            this.age = 0;
        }
    }

    // конструктор копирования
    public Human(Human that) {
        this.firstName = that.firstName;
        this.lastName = that.lastName;
        this.age = that.age;
    }

    // геттер
    public int getAge() {
        return age;
    }

    // сеттер
    public void setAge(int age) {
        if (age >= 0 && age <= 99) {
            this.age = age;
        } else {
            this.age = 0;
        }
    }
}
