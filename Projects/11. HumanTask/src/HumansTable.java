/**
 * 18.02.2021
 * 11. HumanTask
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HumansTable {
    public void printTable(Human humans[]) {
        System.out.printf("%15s|%15s|%10s|\n", "Имя", "Фамилия", "Возраст");
        System.out.println("-------------------------------------------");

        for (int i = 0; i < humans.length; i++) {
            System.out.printf("%15s|%15s|%10d|\n", humans[i].firstName,
                    humans[i].lastName, humans[i].getAge());
        }
        System.out.println("-------------------------------------------");
    }

    public void printTable(int ages[]) {
        System.out.printf("%10s|%12s\n", "Возраст", "Сколько раз");
        System.out.println("-----------------------");
        for (int i = 0; i < ages.length; i++) {
            if (ages[i] != 0) {
                System.out.printf("%10d|%12d\n", i, ages[i]);
            }
        }
    }
}
