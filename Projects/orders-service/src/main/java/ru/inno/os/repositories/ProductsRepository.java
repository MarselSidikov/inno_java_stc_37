package ru.inno.os.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.os.models.Product;

import java.util.List;

/**
 * 21.05.2021
 * orders-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ProductsRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByPriceGreaterThan(Double priceProm);
}
