package ru.inno.os.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.inno.os.models.Product;
import ru.inno.os.repositories.ProductsRepository;

import java.util.List;

/**
 * 21.05.2021
 * orders-service
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@RestController
public class ProductsController {

    @Autowired
    private ProductsRepository productsRepository;

    @GetMapping("/products")
    public List<Product> getAll(@RequestParam("priceFrom") Double priceFrom) {
        return productsRepository.findAllByPriceGreaterThan(priceFrom);
    }

    @PostMapping("/products")
    public Product add(@RequestBody Product product) {
        productsRepository.save(product);
        return product;
    }
}
