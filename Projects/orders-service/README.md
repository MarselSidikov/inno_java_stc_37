# Что нужно знать, чтобы это написать?

1) Основные моменты, касающиеся протокола HTTP - протокол.

2) JSON - формат обмена данными между приложениями.

3) ORM - отображение объектов в таблицы и наоборот.

4) Hibernate - ORM-фреймворк

5) JPA - набор интерфейсов и аннотаций (@Entity, @Id, @Column)

6) Spring - DI/IoC, MVC/WEB - @RestController, @PostMapping, @GetMapping

7) Spring Data JPA - JpaRepository

8) Spring Boot