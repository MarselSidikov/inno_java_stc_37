/**
 * 24.02.2021
 * 16. Builder
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Example {
    private String value = "";

    public Example append(String another) {
        this.value += another;
        return this;
    }

    public String getValue() {
        return value;
    }
}
