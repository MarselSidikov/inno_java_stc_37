/**
 * 24.02.2021
 * 16. Builder
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User {
    private String firstName;
    private String lastName;
    private int age; // 0
    private boolean isWorker; // false

    public User(String firstName, String lastName, int age, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isWorker = isWorker;
    }

    public User(String firstName, String lastName, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

    public User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
