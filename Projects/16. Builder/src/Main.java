public class Main {

    public static void main(String[] args) {
        User user = new User("Марсель", "Сидиков", 27, true);
        // что, если кто-то не захочет указывать возраст?
        User user1 = new User("Марсель", "Сидиков", true);
        // что, если кто-то не захочет указывать свою занятость?
        User user2 = new User("Марсель", "Сидиков", 27);
        // что, если кто-то не захочет указывать и занятость и возраст?
        // нужно сделать еще +100500 конструкторов...

        Example ex1 = new Example();
        ex1.append("Hello!")
                .append("Marsel")
                .append("Bye!");

        System.out.println(ex1.getValue());
//        Example ex2 = ex1.append("Hello!");
//        Example ex3 = ex2.append("Bye");
//        Example ex4 = ex3.append("Marsel");
//        System.out.println(ex4.getValue());
//        System.out.println(ex1.getValue());

    }
}
