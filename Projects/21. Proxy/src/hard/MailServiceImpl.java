package hard;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailServiceImpl implements MailService {
    public void sendMessage(String email) {
        System.out.println("Направлено на email " + email);
    }
}
