package hard;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UserService {
    public void signUp(String email, String password) {
        System.out.println("Регистрация прошла успешно!");
    }

    public void signIn(String email, String password) {
        System.out.println("Вход прошел успешно!");
    }

    public void resetPassword(String email) {
        System.out.println("Заявка на смену пароля отправлена");
    }
}
