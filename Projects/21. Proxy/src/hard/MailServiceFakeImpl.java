package hard;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailServiceFakeImpl implements MailService {
    public void sendMessage(String message) {
        System.out.println("Запрос на отправку принят. Письмо не отправлено.");
    }
}
