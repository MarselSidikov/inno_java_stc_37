package lite;

public class Main {

    public static void main(String[] args) {
        DriverProxy marsel = new DriverProxy("Марсель");

        marsel.setBefore(() -> {
            System.out.println(marsel.getName() + " собрался поехать!");
        });

        marsel.setAfter(() -> {
            System.out.println("А не кукухой ли?");
        });

        marsel.setInstead(() -> {
            System.out.println("Куда-то он поехал...");
        });

//        marsel.drive();

        Driver vicktor = new Driver("Виктор");
        Driver airat = new Driver("Айрат");

        Driver drivers[] = {marsel, vicktor, airat};

        for (int i = 0; i < drivers.length; i++) {
            drivers[i].drive();
        }


    }
}
