package lite;

/**
 * 04.03.2021
 * 21. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface After {
    void execute();
}
