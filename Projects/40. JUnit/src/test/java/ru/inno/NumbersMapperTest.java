package ru.inno;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * 12.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName("NumbersMapper is working when")
@ExtendWith(MockitoExtension.class)
class NumbersMapperTest {

    @Mock
    private NumberToBooleanMapper numberToBooleanMapper;

    private NumbersMapper mapper;

    @BeforeEach
    public void setUp() {
        when(numberToBooleanMapper.map(2)).thenReturn(true);
        when(numberToBooleanMapper.map(3)).thenReturn(false);
        when(numberToBooleanMapper.map(4)).thenReturn(true);
        when(numberToBooleanMapper.map(5)).thenReturn(false);
        mapper = new NumbersMapper(numberToBooleanMapper);
    }

    @ParameterizedTest(name = "return correct for vector {0}")
    @MethodSource("numbers")
    public void return_correct_vector(List<Integer> numbers) {
        List<Boolean> actual = mapper.map(numbers);
        List<Boolean> expected = Arrays.asList(true, false, true, false);
        assertEquals(expected, actual);
    }

    public static Stream<Arguments> numbers() {
        return Stream.of(Arguments.of(Arrays.asList(2, 3, 4, 5)));
    }

}