package ru.inno;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 08.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName(value = "NumberUtils is working when")
public class NumbersUtilTest {
    // объект, который будем тестировать
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("isPrime() is working")
    class ForIsPrime {
        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, 1})
        public void on_problems_numbers_throws_exception(final int incorrectNumber) {
            assertThrows(IncorrectNumberException.class, () -> numbersUtil.isPrime(incorrectNumber));
        }

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 13, 41, 7, 31, 47})
        public void on_prime_numbers_return_true(int primeNumber) {
            assertTrue(numbersUtil.isPrime(primeNumber));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {169, 121, 289})
        public void on_sqrt_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ArgumentsSource(value = CompositeNumbersArgumentsProvider.class)
        public void on_composite_numbers_return_false(int compositeNumber) {
            assertFalse(numbersUtil.isPrime(compositeNumber));
        }
    }

    @Nested
    @DisplayName("gcd() is working")
    class ForGcd {
        @ParameterizedTest(name = "return {2} on number a = {0} and b = {1}")
//        @CsvSource(value = {"18,12,6", "9,12,3", "64,48,16"})
        @CsvFileSource(files = "src\\test\\resources\\gcd.csv")
        public void return_correct_result(int a, int b, int expected) {
            int actual = numbersUtil.gcd(a, b);
            assertEquals(expected, actual);
        }
    }


}
