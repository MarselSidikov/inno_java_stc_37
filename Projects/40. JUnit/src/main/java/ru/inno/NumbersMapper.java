package ru.inno;

import java.util.ArrayList;
import java.util.List;

/**
 * 12.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersMapper {
    private NumberToBooleanMapper mapper;

    public NumbersMapper(NumberToBooleanMapper mapper) {
        this.mapper = mapper;
    }

    List<Boolean> map(List<Integer> numbers) {
        List<Boolean> result = new ArrayList<>();
        for (Integer value : numbers) {
            result.add(mapper.map(value));
        }
        return result;
    }
}
