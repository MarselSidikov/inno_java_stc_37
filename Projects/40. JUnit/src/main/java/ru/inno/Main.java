package ru.inno;

import java.util.Arrays;

/**
 * 08.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        NumbersMapper mapper = new NumbersMapper(numbersUtil);
        System.out.println(mapper.map(Arrays.asList(13, 159, 169, 121, 41)));
    }
}
