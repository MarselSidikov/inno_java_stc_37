package ru.inno;

/**
 * 12.05.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface NumberToBooleanMapper {
    boolean map(int number);
}
