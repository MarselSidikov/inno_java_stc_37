/**
 * 24.02.2021
 * 15. OOP Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// терминал для выдачи денег
public class Terminal {
    private int cash;

    public Terminal(int cash) {
        this.cash = cash;
    }
    // дать деньги человеку human с паролем password в размере sum
    public void giveMoneyTo(Human human, String password, int sum) {
        if (human.getCard().isCorrect(password) && human.getCard().getAmount() >= sum) {
            if (this.cash >= sum) {
                // уменьшили сумму в банкомате
                this.cash -= sum;
                // уменьшили сумму на карте
                human.getCard().setAmount(human.getCard().getAmount() - sum);
            } else {
                System.err.println("Денег в банкомате нет");
            }
        } else {
            System.err.println("Неверный пароль или нет денег на карте");
        }
    }
}
