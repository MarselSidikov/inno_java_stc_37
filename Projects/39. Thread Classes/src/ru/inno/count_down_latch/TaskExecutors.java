package ru.inno.count_down_latch;

/**
 * 19.04.2021
 * 37. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Класс, который будет предоставлять различный реализации TaskExecutor-ов
 */
public class TaskExecutors {

    public static TaskExecutor threadPool(int threadsCount) {
        return new ThreadPoolImpl(threadsCount);
    }
}
