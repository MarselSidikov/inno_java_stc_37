package ru.inno.semaphore;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.Semaphore;

public class Main {

    public static void saveFile(String link) {
        try {
            saveFile0(link);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static void saveFile0(String link) throws Exception {
        URL url = new URL(link);
        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // буфер для изображения
        byte[] buf = new byte[1024];
        int n = 0;
        // считывает блоки байтов в буфер
        while (-1 != (n = in.read(buf))) {
            // потом записывает этот буфер в out
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        // байтовое представление изображения
        byte[] response = out.toByteArray();

        String newFileName = UUID.randomUUID().toString();
        FileOutputStream outputStream = new FileOutputStream("images\\" + newFileName + ".png");
        outputStream.write(response);
        outputStream.close();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Semaphore semaphore = new Semaphore(scanner.nextInt()); // 10

        try {
            BufferedReader reader = new BufferedReader(new FileReader("links.txt"));

            reader.lines().forEach(fileName -> new Thread(() -> {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
                System.out.println(Thread.currentThread().getName() + " начал скачивание");
                saveFile(fileName);
                System.out.println(Thread.currentThread().getName() + " скачал");
                semaphore.release();
            }).start());
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
