class Program3 {

	public static void swap(int a, int b) {
		int temp = a;
		a = b;
		b = temp;
	}

	// overloading
	public static void swap(int array[], int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public static void main(String[] args) {
		/**
		int x = 10;
		int y = 5;
		swap(x, y);

		System.out.println(x + " " + y);
		**/
		int numbers[] = {1, 2, 3, 4, 5, 6};
		swap(numbers, 2, 5);
		System.out.println(numbers[2] + " " + numbers[5]);
	}
}