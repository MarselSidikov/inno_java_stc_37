class Program2 {

	public static boolean isEven(int number) {
		return number % 2 == 0;
	}

	public static boolean isPrime(int number) {
		if (number == 2 || number == 3) {
			return true;
		}

		// for 121 -> 2 ... 11
		for (int i = 2; i * i <= number; i++) {
			if (number % i == 0) {
				return false;
			} 	
		}

		return true;
	}

	public static int getSumOfDigits(int number) {
		int sumOfDigits = 0;

		while(number != 0) {
			sumOfDigits += number % 10;
			number /= 10;
		}

		return sumOfDigits;
	}

	public static void main(String[] args) {
		/**
		int result = getSumOfDigits(777);
		int result2 = getSumOfDigits(888);
		System.out.println(result);	
		System.out.println(result2);
		**/

		boolean a1 = isPrime(7); // true
		boolean a2 = isPrime(121); // false
		boolean a3 = isPrime(167); // true

		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);

		// boolean some = sumOfDigits(555);
	}
}