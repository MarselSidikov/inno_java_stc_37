package inno.hashes;

/**
 * 18.03.2021
 * 28. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static int myHashCode(String string) {
        char array[] = string.toCharArray();
        int code = 0;
        for (int i = 0; i < array.length; i++) {
            code += array[i];
        }
        return code;
    }

    public static int hashCode(String s) {
        int h = 0;
        char value[] = s.toCharArray();

        for (int i = 0; i < value.length; i++) {
            h = 31 * h + value[i];
        }
        return h;
    }

    // 1. h1 = 31 * 0 + 72
    // 2. h2 = 31 * (31 * 0 + 72) + 101
    // 3. h3 = 31 * (31 * (31 * 0 + 72) + 101) + 108
    // 4. h4 = 31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108
    // 5. h5 = 31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111
    // 6. h6 = 31 * (31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111) + 33
    // 31 * 31 * 31 * 31 * 31 * 31 * 0 + 31 * 31 * 31 * 31 * 31 * 72 + 31 * 31 * 31 * 31 * 101 + 31 * 31 * 31 * 108 + 31 * 31 * 108 + 31 * 111 + 33
    // 31^5 * 72 + 31^4 * 101 + 31^3 * 108 + 31^2 * 108 + 31^1 * 111 + 31^0 * 33
    // hash = sum: code * 31^(size - 1 - i)
    // почему степень (size - 1 - i)? - уменьшаем количество коллизий, потому что учитывается позиция символа
    // почему 31? Выбрали это число, потому что оно простое и уменьшает количество коллизий, его нельзя разложить на множители
    // 200 = 100 * 2 = 25 * 4 * 2 = 5 * 5 * 2 * 2 и т.д.
    public static void main(String[] args) {
        String hello = "Hello!"; // H - 72, e - 101, l - 108, l - 108, o = 111, ! = 33
        System.out.println(myHashCode(hello));
        System.out.println(hello.hashCode());
        System.out.println(hashCode(hello));
    }
}
