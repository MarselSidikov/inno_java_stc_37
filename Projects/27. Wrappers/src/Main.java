public class Main {

    // оберточные типы - используются в Generics
    public static void main(String[] args) {
        // boxing
        Integer integer = new Integer(120);
        // unboxing
        int i = integer.intValue();

        //---
        Integer integer1 = 120;
        int i1 = integer1;

        int i2 = Integer.parseInt("1234");

        Character c = 'Ы';
        System.out.println(Character.isDigit(c));
    }
}
