
/**
 * 18.02.2021
 * 12. Bus Project
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// автобус
public class Bus {
    // поля - модель, номер
    private String model;
    private int number;
    // ссылка на водителя, который управляет автобусом
    private Driver driver;

    // массив пассажиров, которые поедут в этом автобусе
    private Passenger passengers[];
    private int passengersCount;

    // конструктор - принимает только модель и номер, потому что изначально
    // мы не знаем, какой водитель будет управлять автобусом
    public Bus(String model, int number) {
        this.model = model;
        this.number = number;
        this.passengers = new Passenger[4];
        this.passengersCount = 0;
    }

    // метод, который позволяет указать водителя для этого автобуса
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    // метод для того, чтобы посадить в автобус пассажира
    public void letPassengerIn(Passenger passenger) {
        if (this.passengersCount < passengers.length) {
            this.passengers[passengersCount] = passenger;
            passengersCount++;
        } else {
            System.err.println("Мест больше нет!");
        }
    }
    // метод, который позволяет автобусу ехать
    public void go() {
        System.out.println("Автобус под номером " + number + " поехал с водителем " + this.driver.getFirstName());
        for (int i = 0; i < passengersCount; i++) {
            System.out.println(passengers[i].getFirstName() + " едет с нами!");
        }
    }
}
