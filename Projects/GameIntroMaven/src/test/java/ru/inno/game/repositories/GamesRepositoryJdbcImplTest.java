package ru.inno.game.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 12.05.2021
 * GameIntroMaven
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
class GamesRepositoryJdbcImplTest {

    private GamesRepositoryJdbcImpl gamesRepository;

    @BeforeEach
    public void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("sql\\schema.sql")
                .addScript("sql\\data.sql")
                .build();
        gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 2})
    void findById(Long id) {
        assertNotNull(gamesRepository.findById(id));
    }
}