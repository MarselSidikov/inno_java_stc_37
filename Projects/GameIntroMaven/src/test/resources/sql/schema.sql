drop table if exists game;
create table game (
    id integer auto_increment primary key,
    datetime varchar(50),
    player_first integer,
    player_second integer
);