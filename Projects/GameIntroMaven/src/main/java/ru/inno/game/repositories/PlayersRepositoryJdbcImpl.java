package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;

/**
 * 26.04.2021
 * GameIntroMaven
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private final static String SQL_FIND_PLAYER_BY_NICKNAME = "select id, ip, name from player where name = ?";

    //language=SQL
    private final static String SQL_INSERT_PLAYER = "insert into player (name, ip) values (?, ?);";

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .build();

    @Override
    public Player findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             // попросим JDBC вернуть нам созданный id-шник
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getName());
            statement.setString(2, player.getIp());
            // affectedRows - сколько строк было изменено в базе данных
            // в случае однократного insert это число должно равняться одному
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert");
            }

            // запросили сгенерированные базой данных ключи
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                // если там что-то есть, давайте это возьмем
                if (generatedKeys.next()) {
                    // мы попросили сгенерированный базой данных id-шник
                    Long id = generatedKeys.getLong("id");
                    player.setId(id);
                } else {
                    // если ключи нам не вернулись
                    throw new SQLException(("Can't retrieve id"));
                }

            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }


        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Player player) {
        System.out.println("MOCK: обновляем IP адрес для " + player.getName());
        // TODO: реализовать
    }
}
