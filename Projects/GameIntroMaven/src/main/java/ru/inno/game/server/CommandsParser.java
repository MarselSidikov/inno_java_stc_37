package ru.inno.game.server;

/**
 * 03.05.2021
 * GameIntroMaven
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CommandsParser {
    public static boolean isMessageForDamage(String messageFromClient) {
        return messageFromClient.equals("DAMAGE");
    }

    public static boolean isMessageForShot(String messageFromClient) {
        return messageFromClient.equals("shot");
    }

    public static boolean isMessageForExit(String messageFromClient) {
        return messageFromClient.equals("exit");
    }

    public static boolean isMessageForNickname(String messageFromClient) {
        return messageFromClient.startsWith("name: ");
    }

    public static boolean isMessageForMove(String messageFromClient) {
        return messageFromClient.equals("left") || messageFromClient.equals("right");
    }

}
