package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

/**
 * 18.04.2021
 * GameIntroMaven
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GamesRepositoryJdbcImpl implements GamesRepository {

    // запросы, которые мы посылаем в базу данных

    // language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game (datetime, player_first, player_second) " +
            " values (?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set second_game_time_amount = ? where id = ?";

    // анонимный класс, реализованный через lambda выражение, который позволяет преобразовать строку из
    // ResultSet в объект Game
    static private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("id"))
            .dateTime(LocalDateTime.parse(row.getString("datetime")))
            .playerFirst(Player.builder()
                    .id(row.getLong("player_first"))
                    .build())
            .playerSecond(Player.builder()
                    .id(row.getLong("player_second"))
                    .build())
            .build();

    // зависимость
    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDateTime().toString());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert");
            }

            // запросили сгенерированные базой данных ключи
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                // если там что-то есть, давайте это возьмем
                if (generatedKeys.next()) {
                    // мы попросили сгенерированный базой данных id-шник
                    Long id = generatedKeys.getLong("id");
                    game.setId(id);
                } else {
                    // если ключи нам не вернулись
                    throw new SQLException(("Can't retrieve id"));
                }

            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setLong(1, game.getSecondsGameTimeAmount());
            statement.setLong(2, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
