import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.models.Game;
import ru.inno.game.repositories.CustomDataSource;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.GamesRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.Scanner;

/**
 * 18.04.2021
 * GameIntroMaven
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/game_db");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("qwerty007");
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println(gamesRepository.findById(1L));
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException();
                }
            }
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println(gamesRepository.findById(1L));
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }).start();

    }
}
