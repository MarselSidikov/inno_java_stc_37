/**
 * 18.02.2021
 * 10. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс
public class Human {
    // поля (fields) - конкретные значения полей
    // определяют состояние объекта
    double height;
    double weight;
    boolean isWorker;

    // методы (methods) - функции и процедуры внутри класса
    // определяют поведение
    void work() {
        isWorker = true;
    }

    void relax() {
        isWorker = false;
    }

    void grow(double value) {
        height += value;
    }

    double getIndex() {
        return height * 100 / weight;
    }
}
