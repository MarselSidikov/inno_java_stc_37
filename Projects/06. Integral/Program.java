import java.util.Scanner;

// http://study-java.ru/uroki-java/formatirovanie-chisel-i-texta-v-java/

class Program {

	public static final double VALUE = 291.67;

	// x^2
	public static double f(double x) {
		return x * x;
	}

	public static double calcIntegral(double a, double b, int n) {
		double h = (b - a) / n;
		
		double integral = 0;

		for (double x = a; x <= b; x += h) {
			double currentRectangle = f(x) * h;
			integral += currentRectangle;
		}	

		return integral;
	}

	public static void calcIntegralsOnSteps(double a, double b, int steps[], double eps[], double ys[]) {
		for (int i = 0; i < steps.length; i++) {
			ys[i] = calcIntegral(a, b, steps[i]);
			eps[i] = ys[i] - VALUE;
		}

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		
		int steps[] = {10, 100, 1_000, 10_000, 100_000, 1_000_000};
		double eps[] = new double[steps.length];
		double ys[] = new double[steps.length];

		calcIntegralsOnSteps(a, b, steps, eps, ys);

		System.out.printf("%7s| %6s| %5s| \n", "N", "y", "eps");
		System.out.println("------------------");
		
		for (int i = 0; i < steps.length; i++) {
			System.out.printf("%7d| %3.3f| %2.3f| \n", steps[i], ys[i], eps[i]);
		}
	
	}
}