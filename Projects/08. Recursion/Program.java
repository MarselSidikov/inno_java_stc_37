class Program {
	public static int f(int n) {
		System.out.println("--> f(" + n + ")");
		if (n == 0) {
			return 1;
		}

		int previous = f(n - 1);
		int result = previous * n;
		System.out.println("<-- f(" + n + ") = "
		 + previous + " * " + n + " = " + result);
		return result;
	}

	public static void main(String[] args) {
		int result = f(5);
		System.out.println(result); // 120
	}
}