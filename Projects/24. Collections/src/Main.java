public class Main {

    public static void main(String[] args) {
        // создал список
        InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);

        System.out.println(list.get(0)); // 7
        System.out.println(list.get(3)); // 12
        System.out.println(list.get(7)); // 100

        InnoIterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

        InnoIterator iterator2 = list.iterator();

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }

    }
}
