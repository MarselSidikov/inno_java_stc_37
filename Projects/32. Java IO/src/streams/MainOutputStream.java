package streams;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutputStream {
    public static void main(String[] args) throws Exception {
        OutputStream outputStream = new FileOutputStream("output.txt", true);
//        outputStream.write("Марсель".getBytes());
        outputStream.write((byte)'П');
        outputStream.close();
    }
}
