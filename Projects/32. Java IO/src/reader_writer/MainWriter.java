package reader_writer;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainWriter {
    public static void main(String[] args) throws Exception {
        Writer writer = new FileWriter("output.txt");
        writer.write('П');
        writer.close();

    }
}
