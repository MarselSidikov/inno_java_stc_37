package reader_writer;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * 29.03.2021
 * 32. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainPrintWriter {
    public static void main(String[] args) throws Exception {
        Writer writer = new FileWriter("output.txt");
        PrintWriter printWriter = new PrintWriter(writer);
        printWriter.printf("%10d + %10d = %10d", 100, 100, 200);
        printWriter.close();
    }
}
