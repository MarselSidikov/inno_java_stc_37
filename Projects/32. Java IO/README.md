# InputStream

```java
abstract class InputStream {
    // -1 - когда поток кончился
    // 0 - 255 значения байтов
    // НО! В java byte - знаковый, следовательно -128 ... 127
    // -1 - признак конца потока или это байт со значением 255?
    public abstract int read() throws IOException;
    
    // здесь возвращается количество считанных байтов, вам уже не важно, -1 или 255
    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }
    
     public int read(byte b[], int off, int len) throws IOException {
        // ...       
    }

}
```

# FileInputStream

```java
public class FileInputStream extends InputStream {

    private final String path;
    
    public int read() throws IOException {
        return read0();
    }
    
    private native int read0() throws IOException;
}
```

Есть класс InputStream, который описывает в общем случае входной поток байтов, есть его реализация FileInputStream, 
в котором нативно реализован абстрактный метод read из класса InputStream. он возвращает один байт из файла.